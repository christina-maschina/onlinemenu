package hr.tvz.android.onlinemenuapp.login;

import hr.tvz.android.onlinemenuapp.model.Restaurant;

public interface LoginContract {
    interface Model {
        interface OnFinishedListener {
            void onCodeError();

            void onSuccess(Restaurant restaurant);
        }

        void login(String code, OnFinishedListener listener);
    }

    interface View {
        void setCodeError();

        void navigateToRestaurant(Restaurant restaurant);
    }

    interface Presenter {
        void validateCode(String code);
    }
}
