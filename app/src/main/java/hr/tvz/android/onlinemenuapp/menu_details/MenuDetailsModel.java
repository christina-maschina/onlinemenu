package hr.tvz.android.onlinemenuapp.menu_details;

import android.os.Bundle;

import hr.tvz.android.onlinemenuapp.model.Dish;

public class MenuDetailsModel implements MenuDetailsContract.Model{
    @Override
    public void getMenuDetails(Bundle bundle, OnFinishedListener listener) {
        if(bundle!=null){
            Dish dish = bundle.getParcelable("dish");
            listener.onFinished(dish);
        }else{
            listener.onFailure("Couldn't load menu details data");
        }
    }
}
