package hr.tvz.android.onlinemenuapp.menu;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import hr.tvz.android.onlinemenuapp.menu_details.MenuDetailsFragment;
import hr.tvz.android.onlinemenuapp.order.OrderActivity;
import hr.tvz.android.onlinemenuapp.R;
import hr.tvz.android.onlinemenuapp.databinding.ActivityMenuBinding;
import hr.tvz.android.onlinemenuapp.model.Dish;
import hr.tvz.android.onlinemenuapp.model.DishType;
import hr.tvz.android.onlinemenuapp.menu.tab.CollectionMenuFragment;
import hr.tvz.android.onlinemenuapp.menu.tab.ObjectMenuFragment;
import hr.tvz.android.onlinemenuapp.restaurant.RestaurantActivity;

public class MenuActivity extends AppCompatActivity implements ObjectMenuFragment.OnItemSelectedListener,
        MenuContract.View, View.OnClickListener {
    private final String TAG = "MenuActivity";
    private ActivityMenuBinding binding;
    public static Button checkout_btn;
    public static double price = 0.0;
    public static ArrayList<Dish> dishList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMenuBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle(RestaurantActivity.RESTAURANT_NAME);

        checkout_btn = binding.checkoutBtn;
        binding.checkoutBtn.setOnClickListener(this);
        if(!dishList.isEmpty()){
            checkout_btn.setVisibility(View.VISIBLE);
            checkout_btn.setText(getString(R.string.checkout) + "                        " + String.valueOf(price));
        }

        MenuPresenter menuPresenter = new MenuPresenter(this);
        menuPresenter.requestDishTypesData();

    }

    @Override
    public void OnItemSelected(Dish dish) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("dish", dish);
        MenuDetailsFragment detailsFragment = new MenuDetailsFragment();
        detailsFragment.setArguments(arguments);
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, detailsFragment)
                .commit();
    }

    public void setData(ArrayList<Dish> data, double price) {
        this.dishList = data;
        MenuActivity.price = price;
    }

    @Override
    public void setMenuFragment(List<DishType> dishTypes) {
        CollectionMenuFragment fragment = new CollectionMenuFragment(dishTypes);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public void onResponseFailure(Throwable t) {
        Log.d(TAG, t.getMessage());
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MenuActivity.this, OrderActivity.class);
        intent.putParcelableArrayListExtra("dishes", dishList);
        intent.putExtra("price", price);
        ActivityOptions options = ActivityOptions.makeScaleUpAnimation(v, 0, 0, v.getWidth(), v.getHeight());
        startActivity(intent, options.toBundle());
    }

}