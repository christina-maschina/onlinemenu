package hr.tvz.android.onlinemenuapp.order;

import android.content.Intent;

import java.util.List;

public class OrderPresenter implements OrderContract.Presenter, OrderContract.Model.OnFinishedListener {
    private OrderContract.View orderView;
    private OrderModel orderModel;

    public OrderPresenter(OrderContract.View orderView) {
        this.orderView = orderView;
        orderModel=new OrderModel();
    }

    @Override
    public void onFinished(List<String> names, List<String> prices, double finalPrice) {
        orderView.setDataToView(names, prices, finalPrice);
    }

    @Override
    public void onFailure(String error) {
        orderView.onResponseFailure(error);
    }

    @Override
    public void requestOrderData(Intent intent) {
        orderModel.getOrderData(intent, this);
    }

    @Override
    public void saveOrderData() {
        orderModel.saveOrderData(this);
    }

    @Override
    public void deleteOrderData(int position) {
        orderModel.deleteOrderData(this, position);
    }

    @Override
    public void showToast(int message, int num) {
        orderView.onResponseSuccess(message, num);
    }
}
