package hr.tvz.android.onlinemenuapp.login;

import android.util.Log;

import hr.tvz.android.onlinemenuapp.model.Restaurant;
import hr.tvz.android.onlinemenuapp.model.User;
import hr.tvz.android.onlinemenuapp.network.Service;
import hr.tvz.android.onlinemenuapp.network.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginModel implements LoginContract.Model {
    private final String TAG = "LoginModel";

    @Override
    public void login(String code, OnFinishedListener listener) {
        Service service = ServiceGenerator.createService(Service.class);
        Call<User> call_auth = service.authenticate();
        call_auth.enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Call<Restaurant> call_restaurant = service.getRestaurant(Integer.valueOf(code));
                call_restaurant.enqueue(new Callback<Restaurant>() {

                    @Override
                    public void onResponse(Call<Restaurant> call, Response<Restaurant> response) {
                        if (response.code() == 404) {
                            listener.onCodeError();
                        } else {
                            listener.onSuccess(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<Restaurant> call, Throwable t) {
                        Log.d(TAG, t.getMessage());
                    }
                });
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });

    }
}
