package hr.tvz.android.onlinemenuapp.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import hr.tvz.android.onlinemenuapp.R;
import hr.tvz.android.onlinemenuapp.restaurant.RestaurantActivity;
import hr.tvz.android.onlinemenuapp.databinding.ActivityMainBinding;
import hr.tvz.android.onlinemenuapp.model.Restaurant;

public class MainActivity extends AppCompatActivity implements LoginContract.View, View.OnClickListener {
    public static long RESTAURANT_ID;
    private EditText waiter_code;
    private LoginPresenter loginPresenter;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        view = binding.getRoot();
        setContentView(view);

        waiter_code = binding.code;
        binding.loginButton.setOnClickListener(this);

        loginPresenter = new LoginPresenter(this);
    }

    @Override
    public void setCodeError() {
        Toast.makeText(getApplicationContext(), R.string.wrongCodeToast, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToRestaurant(Restaurant restaurant) {
        RESTAURANT_ID = restaurant.getId();
        Intent intent = new Intent(MainActivity.this, RestaurantActivity.class);
        intent.putExtra("restaurant", restaurant);
        ActivityOptions options = ActivityOptions.makeScaleUpAnimation(view, 0, 0, view.getWidth(), view.getHeight());
        startActivity(intent, options.toBundle());
    }

    @Override
    public void onClick(View v) {
        loginPresenter.validateCode(waiter_code.getText().toString());
    }
}