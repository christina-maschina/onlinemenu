package hr.tvz.android.onlinemenuapp.restaurant;

import android.content.Intent;
import hr.tvz.android.onlinemenuapp.model.Restaurant;

public class RestaurantModel implements RestaurantContract.Model {
    @Override
    public void getRestaurant(Intent intent, OnFinishedListener listener) {
        if (intent != null) {
            Restaurant restaurant = intent.getParcelableExtra("restaurant");
            listener.onFinished(restaurant);
        } else {
            listener.onFailure("Couldn't load restaurant data");
        }

    }
}
