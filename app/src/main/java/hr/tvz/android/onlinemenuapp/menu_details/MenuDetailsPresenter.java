package hr.tvz.android.onlinemenuapp.menu_details;

import android.os.Bundle;

import hr.tvz.android.onlinemenuapp.model.Dish;

public class MenuDetailsPresenter implements MenuDetailsContract.Presenter, MenuDetailsContract.Model.OnFinishedListener {
    private MenuDetailsContract.View menuDetailsView;
    private MenuDetailsModel menuDetailsModel;

    public MenuDetailsPresenter(MenuDetailsContract.View menuDetailsView) {
        this.menuDetailsView = menuDetailsView;
        menuDetailsModel=new MenuDetailsModel();
    }

    @Override
    public void onFinished(Dish dish) {
        menuDetailsView.setDataToView(dish);
    }

    @Override
    public void onFailure(String error) {
        menuDetailsView.onResponseFailure(error);
    }

    @Override
    public void requestMenuDetails(Bundle bundle) {
        menuDetailsModel.getMenuDetails(bundle, this);
    }
}
