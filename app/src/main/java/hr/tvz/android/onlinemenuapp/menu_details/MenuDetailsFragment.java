package hr.tvz.android.onlinemenuapp.menu_details;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import hr.tvz.android.onlinemenuapp.R;
import hr.tvz.android.onlinemenuapp.menu.MenuActivity;
import hr.tvz.android.onlinemenuapp.model.Dish;

import static hr.tvz.android.onlinemenuapp.menu.MenuActivity.checkout_btn;
import static hr.tvz.android.onlinemenuapp.menu.MenuActivity.price;

public class MenuDetailsFragment extends Fragment implements MenuDetailsContract.View {
    private final String TAG = "MenuDetailsFragment";
    private TextView titleView, descriptionView, priceView;
    private ImageView imageView;
    private Button order_btn;
    private ConstraintLayout constraintLayout;

    public MenuDetailsFragment() { }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_fragment, container, false);
        titleView = view.findViewById(R.id.dish_name);
        descriptionView = view.findViewById(R.id.dish_description);
        imageView = view.findViewById(R.id.dish_img);
        priceView = view.findViewById(R.id.dish_price);
        order_btn = view.findViewById(R.id.order_btn);
        constraintLayout = view.findViewById(R.id.details_fragment);

        MenuDetailsPresenter menuDetailsPresenter = new MenuDetailsPresenter(this);
        menuDetailsPresenter.requestMenuDetails(getArguments());

        return view;
    }

    @Override
    public void setDataToView(Dish dish) {
        if (dish.getDescription().isEmpty() && dish.getImagePath().isEmpty()) {
            descriptionView.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
            titleView.setGravity(Gravity.CENTER_HORIZONTAL);
            priceView.setGravity(Gravity.CENTER_HORIZONTAL);
            if (getContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                alignTextView();
            }
        }
        titleView.setText(dish.getName());
        descriptionView.setText(dish.getDescription());
        priceView.setText(String.format("%s HRK", dish.getPrice().toString()));
        Picasso.get()
                .load(Uri.parse(dish.getImagePath()))
                .fit()
                .centerCrop()
                .into(imageView);

        order_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuActivity.dishList.add(dish);
                price = dish.getPrice() + price;
                MenuActivity.checkout_btn.setVisibility(View.VISIBLE);
                MenuActivity.checkout_btn.setText(getString(R.string.checkout) + "                        " + String.valueOf(price));
                ObjectAnimator colorAnim = ObjectAnimator.ofInt(MenuActivity.checkout_btn, "textColor", Color.RED, Color.WHITE);
                colorAnim.setDuration(1500);
                colorAnim.setEvaluator(new ArgbEvaluator());
                colorAnim.start();
            }
        });
    }


    @Override
    public void onResponseFailure(String error) {
        Log.d(TAG, error);
    }

    private void alignTextView() {
        titleView.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        priceView.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(constraintLayout);
        constraintSet.connect(R.id.dish_name, ConstraintSet.START, R.id.details_fragment, ConstraintSet.START, 0);
        constraintSet.connect(R.id.dish_name, ConstraintSet.END, R.id.details_fragment, ConstraintSet.END, 0);
        constraintSet.connect(R.id.dish_name, ConstraintSet.TOP, R.id.details_fragment, ConstraintSet.TOP, 0);
        constraintSet.connect(R.id.dish_name, ConstraintSet.BOTTOM, R.id.dish_price, ConstraintSet.BOTTOM, 0);

        constraintSet.connect(R.id.dish_price, ConstraintSet.START, R.id.details_fragment, ConstraintSet.START, 0);
        constraintSet.connect(R.id.dish_price, ConstraintSet.END, R.id.details_fragment, ConstraintSet.END, 0);
        constraintSet.connect(R.id.dish_price, ConstraintSet.TOP, R.id.dish_name, ConstraintSet.BOTTOM, 0);
        constraintSet.connect(R.id.dish_price, ConstraintSet.BOTTOM, R.id.order_btn, ConstraintSet.TOP, 0);

        constraintSet.connect(R.id.order_btn, ConstraintSet.START, R.id.details_fragment, ConstraintSet.START, 0);
        constraintSet.connect(R.id.order_btn, ConstraintSet.END, R.id.details_fragment, ConstraintSet.END, 0);
        constraintSet.connect(R.id.order_btn, ConstraintSet.TOP, R.id.dish_price, ConstraintSet.BOTTOM, 0);
        constraintSet.connect(R.id.order_btn, ConstraintSet.BOTTOM, R.id.details_fragment, ConstraintSet.BOTTOM, 0);
        constraintSet.connect(R.id.checkout_btn, ConstraintSet.BOTTOM, R.id.details_fragment, ConstraintSet.BOTTOM, 0);

        constraintSet.applyTo(constraintLayout);
    }

}
