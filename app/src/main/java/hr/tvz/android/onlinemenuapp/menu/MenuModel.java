package hr.tvz.android.onlinemenuapp.menu;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

import hr.tvz.android.onlinemenuapp.login.MainActivity;
import hr.tvz.android.onlinemenuapp.model.Dish;
import hr.tvz.android.onlinemenuapp.model.DishType;
import hr.tvz.android.onlinemenuapp.network.Service;
import hr.tvz.android.onlinemenuapp.network.ServiceGenerator;
import hr.tvz.android.onlinemenuapp.menu.tab.ObjectMenuFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuModel implements MenuContract.Model {
    private final String TAG = "MenuModel";

    @Override
    public void getDishTypes(OnFinishedListener listener) {
        Service service = ServiceGenerator.createService(Service.class);
        Call<List<DishType>> call = service.getDishTypes(MainActivity.RESTAURANT_ID);
        call.enqueue(new Callback<List<DishType>>() {
            @Override
            public void onResponse(Call<List<DishType>> call, Response<List<DishType>> response) {
                listener.onFinishedDishTypes(response.body());
            }

            @Override
            public void onFailure(Call<List<DishType>> call, Throwable t) {
                listener.onFailure(t);
            }
        });

    }

    @Override
    public void getDishes(Bundle bundle, OnFinishedListener listener) {
        Service service = ServiceGenerator.createService(Service.class);
        Call<List<Dish>> call = service.getDishes(MainActivity.RESTAURANT_ID, bundle.getLong(ObjectMenuFragment.ARG_OBJECT));
        call.enqueue(new Callback<List<Dish>>() {
            @Override
            public void onResponse(Call<List<Dish>> call, Response<List<Dish>> response) {
                Log.d(TAG, response.body().toString());
                List<Dish> dishes = response.body();
                listener.onFinishedDishes(dishes);
            }

            @Override
            public void onFailure(Call<List<Dish>> call, Throwable t) {
                listener.onFailure(t);
            }
        });
    }
}
