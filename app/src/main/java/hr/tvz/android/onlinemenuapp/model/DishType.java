package hr.tvz.android.onlinemenuapp.model;

import com.google.gson.annotations.SerializedName;

public class DishType {
    @SerializedName("id")
    long id;
    @SerializedName("name")
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
