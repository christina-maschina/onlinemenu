package hr.tvz.android.onlinemenuapp.menu.tab;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.List;

import hr.tvz.android.onlinemenuapp.R;
import hr.tvz.android.onlinemenuapp.dialog.CallWaiterDialog;
import hr.tvz.android.onlinemenuapp.model.DishType;

public class CollectionMenuFragment extends Fragment {
    private CollectionMenuAdapter menuCollectionAdapter;
    private ViewPager2 viewPager;
    private List<DishType> dishTypes;

    public CollectionMenuFragment() { }

    public CollectionMenuFragment(List<DishType> body) {
        this.dishTypes = body;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null){
            setHasOptionsMenu(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.collection_menu_tab, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        menuCollectionAdapter = new CollectionMenuAdapter(this, dishTypes);
        viewPager = view.findViewById(R.id.pager_tab);
        viewPager.setAdapter(menuCollectionAdapter);

        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        new TabLayoutMediator(tabLayout, viewPager,
                (tab, position) -> tab.setText(dishTypes.get(position).getName())
        ).attach();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint(getString(R.string.search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                ObjectMenuFragment.adapter.getFilter().filter(s);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (R.id.menu_remove == item.getItemId()) {
            DialogFragment dialogFragment = new CallWaiterDialog(getActivity());
            dialogFragment.show(getActivity().getSupportFragmentManager(), "help");
        }

        return super.onOptionsItemSelected(item);
    }

}
