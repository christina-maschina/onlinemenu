package hr.tvz.android.onlinemenuapp.network;

import java.util.List;

import hr.tvz.android.onlinemenuapp.model.Dish;
import hr.tvz.android.onlinemenuapp.model.DishType;
import hr.tvz.android.onlinemenuapp.model.Order;
import hr.tvz.android.onlinemenuapp.model.Restaurant;
import hr.tvz.android.onlinemenuapp.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Service {

    @GET("user")
    Call<User> authenticate();

    @GET("restaurant/{code}")
    Call<Restaurant> getRestaurant(@Path("code") Integer code);

    @GET("dish/{id}")
    Call <List<DishType>> getDishTypes (@Path("id") Long id);

    @GET("dish")
    Call <List<Dish>> getDishes (@Query("restaurantId") Long restaurantId, @Query("dishTypeId") Long dishTypeId);

    @POST("order")
    Call<Order> saveOrder(@Body Order order);
}
