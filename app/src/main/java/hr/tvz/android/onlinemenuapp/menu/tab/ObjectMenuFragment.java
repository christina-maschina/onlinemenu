package hr.tvz.android.onlinemenuapp.menu.tab;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hr.tvz.android.onlinemenuapp.menu.MenuRecyclerViewAdapter;
import hr.tvz.android.onlinemenuapp.R;
import hr.tvz.android.onlinemenuapp.menu.MenuContract;
import hr.tvz.android.onlinemenuapp.menu.MenuPresenter;
import hr.tvz.android.onlinemenuapp.model.Dish;

public class ObjectMenuFragment extends Fragment implements MenuContract.ObjectView {
    private final String TAG = "MenuObjectFragment";
    public static final String ARG_OBJECT = "id";
    private RecyclerView recyclerView;
    public static MenuRecyclerViewAdapter adapter;
    private Context context;
    OnItemSelectedListener listener;

    public ObjectMenuFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (!(context instanceof OnItemSelectedListener)) {
            throw new ClassCastException(context.toString() + getString(R.string.listener));
        }
        listener = (OnItemSelectedListener) context;
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ui_fragment, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        MenuPresenter menuPresenter = new MenuPresenter(this);
        menuPresenter.requestDishesData(getArguments());
    }

    @Override
    public void setDataToRecyclerView(List<Dish> dishes) {
        Log.d(TAG, String.valueOf(dishes));
        adapter = new MenuRecyclerViewAdapter(dishes, context, listener);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public interface OnItemSelectedListener {
        void OnItemSelected(Dish dish);
    }
}