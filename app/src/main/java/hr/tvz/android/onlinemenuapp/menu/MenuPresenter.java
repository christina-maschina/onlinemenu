package hr.tvz.android.onlinemenuapp.menu;

import android.os.Bundle;

import java.util.List;

import hr.tvz.android.onlinemenuapp.model.Dish;
import hr.tvz.android.onlinemenuapp.model.DishType;

public class MenuPresenter implements MenuContract.Presenter, MenuContract.Model.OnFinishedListener {
    MenuContract.View menuView;
    MenuContract.ObjectView objectView;
    MenuModel menuModel;

    public MenuPresenter(MenuContract.View menuView) {
        this.menuView = menuView;
        menuModel = new MenuModel();
    }

    public MenuPresenter(MenuContract.ObjectView objectView) {
        this.objectView = objectView;
        menuModel = new MenuModel();
    }

    @Override
    public void onFinishedDishTypes(List<DishType> dishTypes) {
        menuView.setMenuFragment(dishTypes);
    }

    @Override
    public void onFinishedDishes(List<Dish> dishes) {
        objectView.setDataToRecyclerView(dishes);
    }

    @Override
    public void onFailure(Throwable t) {
        menuView.onResponseFailure(t);
    }

    @Override
    public void requestDishTypesData() {
        menuModel.getDishTypes(this);
    }

    @Override
    public void requestDishesData(Bundle bundle) {
        menuModel.getDishes(bundle, this);
    }
}
