package hr.tvz.android.onlinemenuapp.model;

import com.google.gson.annotations.SerializedName;


public class User {

    @SerializedName("status")
    String status;
    @SerializedName("id")
    long  id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
