package hr.tvz.android.onlinemenuapp.menu;

import android.os.Bundle;

import java.util.List;

import hr.tvz.android.onlinemenuapp.model.Dish;
import hr.tvz.android.onlinemenuapp.model.DishType;

public interface MenuContract {
    interface Model{
        interface OnFinishedListener{
            void onFinishedDishTypes(List<DishType> dishType);
            void onFinishedDishes(List<Dish> dishes);
            void onFailure(Throwable t);
        }
        void getDishTypes(OnFinishedListener listener);
        void getDishes(Bundle bundle, OnFinishedListener listener);
    }
    interface View{
        void setMenuFragment(List<DishType> dishType);
        void onResponseFailure(Throwable t);
    }
    interface ObjectView{
        void setDataToRecyclerView(List<Dish> dishes);
    }
    
    interface Presenter{
        void requestDishTypesData();
        void requestDishesData(Bundle bundle);
    }
}
