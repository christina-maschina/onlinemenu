package hr.tvz.android.onlinemenuapp.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.shape.RoundedCornerTreatment;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

import hr.tvz.android.onlinemenuapp.R;
import hr.tvz.android.onlinemenuapp.model.Dish;
import hr.tvz.android.onlinemenuapp.menu.tab.ObjectMenuFragment;

import static java.lang.Math.round;

public class MenuRecyclerViewAdapter extends RecyclerView.Adapter<MenuRecyclerViewAdapter.ViewHolder> implements Filterable {
    private final String TAG="MenuRecyclerViewAdapter";
    private List<Dish> dishes;
    private List<Dish> dishesListSearch;
    private Context context;
    ObjectMenuFragment.OnItemSelectedListener listener;

    public MenuRecyclerViewAdapter(List<Dish> dishList, Context context, ObjectMenuFragment.OnItemSelectedListener listener) {
        dishes = dishList;
        this.context = context;
        this.dishesListSearch = new ArrayList<>(dishList);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.recyclerview_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nameTextView.setText(dishes.get(position).getName());
        if (dishes.get(position).getImagePath().isEmpty() && dishes.get(position).getDescription().isEmpty()) {
            holder.imageView.setVisibility(View.GONE);
            holder.descriptionTextView.setVisibility(View.GONE);
            holder.priceTextView.setVisibility(View.GONE);
            holder.nameTextView.setGravity(Gravity.CENTER_HORIZONTAL);

        } else {
            holder.descriptionTextView.setText(dishes.get(position).getDescription());
            holder.priceTextView.setText(String.format("%s HRK", dishes.get(position).getPrice()));
            if(context.getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT){
                Picasso
                        .get()
                        .load(Uri.parse(dishes.get(position).getImagePath()))
                        .resize(400, 250)
                        .into(holder.imageView);
            }else{
                Picasso
                        .get()
                        .load(Uri.parse(dishes.get(position).getImagePath()))
                        .resize(690, 350)
                        .into(holder.imageView);
            }

        }

        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnItemSelected(dishes.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return dishes.size();
    }

    @Override
    public Filter getFilter() {
        return dishFilter;
    }

    private final Filter dishFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Dish> filteredList = new ArrayList<>();
            Log.d(TAG, String.valueOf(filteredList));

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dishesListSearch);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Dish dish : dishesListSearch) {
                    if (dish.getName().toLowerCase().contains(filterPattern)) {
                        Log.d(TAG, dish.getName());
                        filteredList.add(dish);
                    }
                }
            }
            Log.d(TAG, String.valueOf(filteredList));

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dishes.clear();
            dishes.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView nameTextView;
        TextView descriptionTextView;
        TextView priceTextView;
        ConstraintLayout constraintLayout;

        ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img);
            nameTextView = itemView.findViewById(R.id.name);
            descriptionTextView = itemView.findViewById(R.id.description);
            priceTextView = itemView.findViewById(R.id.price);
            constraintLayout = itemView.findViewById(R.id.mainLayout);
        }

    }
}
