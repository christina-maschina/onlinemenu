package hr.tvz.android.onlinemenuapp.restaurant;

import android.content.Intent;

import hr.tvz.android.onlinemenuapp.model.Restaurant;

public class RestaurantPresenter implements RestaurantContract.Presenter, RestaurantContract.Model.OnFinishedListener {
   RestaurantContract.View restaurantView;
   RestaurantModel restaurantModel;

    public RestaurantPresenter(RestaurantContract.View restaurantView) {
        this.restaurantView = restaurantView;
        restaurantModel=new RestaurantModel();
    }

    @Override
    public void onFinished(Restaurant restaurant) {
        restaurantView.setDataToView(restaurant);
    }

    @Override
    public void onFailure(String error) {
        restaurantView.onResponseFailure(error);
    }

    @Override
    public void requestRestaurantData(Intent intent) {
        restaurantModel.getRestaurant(intent, this);
    }
}
