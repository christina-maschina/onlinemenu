package hr.tvz.android.onlinemenuapp.restaurant;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import hr.tvz.android.onlinemenuapp.menu.MenuActivity;
import hr.tvz.android.onlinemenuapp.databinding.ActivityRestaurantBinding;
import hr.tvz.android.onlinemenuapp.model.Restaurant;


public class RestaurantActivity extends AppCompatActivity implements RestaurantContract.View, View.OnClickListener {
    private final String TAG = "RestaurantActivity";
    private ImageView menuImg;
    private TextView restaurantName;
    private TextView restaurantDescription;
    public static String RESTAURANT_NAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityRestaurantBinding binding = ActivityRestaurantBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        menuImg = binding.menuImg;
        restaurantName = binding.restaurantName;
        restaurantDescription = binding.restaurantDescription;
        binding.menuBtn.setOnClickListener(this);

        RestaurantPresenter restaurantPresenter = new RestaurantPresenter(this);
        restaurantPresenter.requestRestaurantData(getIntent());
    }

    @Override
    public void setDataToView(Restaurant restaurant) {
        RESTAURANT_NAME=restaurant.getName();
        restaurantName.setText(RESTAURANT_NAME);
        restaurantDescription.setText(restaurant.getDescription());

        Picasso.get().setLoggingEnabled(true);
        Picasso.get()
                .load(Uri.parse(restaurant.getImage()))
                .fit()
                .centerCrop()
                .into(menuImg);
    }

    @Override
    public void onResponseFailure(String error) {
        Log.d(TAG, error);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(RestaurantActivity.this, MenuActivity.class);
        ActivityOptions options=ActivityOptions.makeScaleUpAnimation(v, 0, 0, v.getWidth(), v.getHeight());
        startActivity(intent, options.toBundle());
    }
}