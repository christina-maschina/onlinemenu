package hr.tvz.android.onlinemenuapp.restaurant;

import android.content.Intent;

import hr.tvz.android.onlinemenuapp.model.Restaurant;

public interface RestaurantContract {
    interface Model {
        interface OnFinishedListener{
            void onFinished(Restaurant restaurant);
            void onFailure(String error);
        }
        void getRestaurant(Intent intent, OnFinishedListener listener);
    }
    interface View{
        void setDataToView(Restaurant restaurant);
        void onResponseFailure(String error);
    }

    interface Presenter{
        void requestRestaurantData(Intent intent);
    }
}
