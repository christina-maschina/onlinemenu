package hr.tvz.android.onlinemenuapp.login;

import hr.tvz.android.onlinemenuapp.model.Restaurant;

public class LoginPresenter implements LoginContract.Presenter, LoginContract.Model.OnFinishedListener {
    private LoginContract.View loginView;
    private LoginModel loginModel;

    public LoginPresenter(LoginContract.View loginView) {
        this.loginView = loginView;
        loginModel = new LoginModel();
    }

    @Override
    public void validateCode(String code) {
        loginModel.login(code, this);
    }

    @Override
    public void onCodeError() {
        loginView.setCodeError();
    }

    @Override
    public void onSuccess(Restaurant restaurant) {
        loginView.navigateToRestaurant(restaurant);
    }
}
