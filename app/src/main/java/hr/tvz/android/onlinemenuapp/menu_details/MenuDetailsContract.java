package hr.tvz.android.onlinemenuapp.menu_details;

import android.os.Bundle;

import hr.tvz.android.onlinemenuapp.model.Dish;

public interface MenuDetailsContract {
    interface Model{
        interface OnFinishedListener{
            void onFinished(Dish dish);
            void onFailure(String error);
        }
        void getMenuDetails(Bundle bundle, OnFinishedListener listener);
    }
    interface View{
        void setDataToView(Dish dish);
        void onResponseFailure(String error);
    }

    interface Presenter{
        void requestMenuDetails(Bundle bundle);
    }
}
