package hr.tvz.android.onlinemenuapp.menu.tab;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;
import java.util.List;

import hr.tvz.android.onlinemenuapp.model.DishType;


public class CollectionMenuAdapter extends FragmentStateAdapter {
    private List<DishType> dishTypes;

    public CollectionMenuAdapter(Fragment fragment, List<DishType> dishTypes) {
        super(fragment);
        this.dishTypes = dishTypes;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment = new ObjectMenuFragment();
        Bundle args = new Bundle();
        args.putLong(ObjectMenuFragment.ARG_OBJECT, dishTypes.get(position).getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getItemCount() {
        return dishTypes == null ? 0 : dishTypes.size();
    }


}
