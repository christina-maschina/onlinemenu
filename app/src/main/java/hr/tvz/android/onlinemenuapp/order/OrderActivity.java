package hr.tvz.android.onlinemenuapp.order;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hr.tvz.android.onlinemenuapp.R;
import hr.tvz.android.onlinemenuapp.databinding.ActivityOrderBinding;
import hr.tvz.android.onlinemenuapp.login.MainActivity;
import hr.tvz.android.onlinemenuapp.menu.MenuActivity;
import hr.tvz.android.onlinemenuapp.restaurant.RestaurantActivity;

import static hr.tvz.android.onlinemenuapp.menu.MenuActivity.price;

public class OrderActivity extends AppCompatActivity implements OrderContract.View, View.OnClickListener {
    private final String TAG = "OrderActivity";
    ListView lists;
    ListView prices;
    private TextView final_price;
    private OrderPresenter orderPresenter;
    private ActivityOrderBinding binding;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrderBinding.inflate(getLayoutInflater());
        view = binding.getRoot();
        setContentView(view);

        getSupportActionBar().setTitle(RestaurantActivity.RESTAURANT_NAME);

        lists = binding.lists;
        prices = binding.prices;
        final_price = binding.finalPrice;
        binding.confirmBtn.setOnClickListener(this);

        orderPresenter = new OrderPresenter(this);
        orderPresenter.requestOrderData(getIntent());

        delete();

    }

    private void delete() {
        lists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MenuActivity.dishList.remove(position);
                orderPresenter.deleteOrderData(position);
            }
        });
    }

    @Override
    public void setDataToView(List<String> names, List<String> prices, double finalPrice) {
        ArrayAdapter<String> adapterList = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names);
        ArrayAdapter<String> adapterPrice = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, prices);
        lists.setAdapter(adapterList);
        this.prices.setAdapter(adapterPrice);

        price = finalPrice;
        final_price.setText(String.valueOf(finalPrice));

        if (finalPrice == 0.0) {
            MenuActivity.checkout_btn.setVisibility(View.INVISIBLE);
        } else {
            MenuActivity.checkout_btn.setText(getString(R.string.checkout) + "                        " + String.valueOf(finalPrice));
        }

    }

    @Override
    public void onResponseFailure(String error) {
        Log.d(TAG, error);
    }

    @Override
    public void onResponseSuccess(int message, int num) {
        Toast.makeText(OrderActivity.this, message, Toast.LENGTH_SHORT).show();
        if (num == 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(OrderActivity.this, MainActivity.class);
                    ActivityOptions options = ActivityOptions.makeScaleUpAnimation(view, 0, 0, view.getWidth(), view.getHeight());
                    startActivity(intent, options.toBundle());
                }
            }, 2000);
        }
    }


    @Override
    public void onClick(View v) {
        orderPresenter.saveOrderData();
        MenuActivity.price = 0.0;
        MenuActivity.dishList = new ArrayList<>();
        MenuActivity.checkout_btn.setText(R.string.checkout);
        MenuActivity.checkout_btn.setVisibility(View.INVISIBLE);
    }
}