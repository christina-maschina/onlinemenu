package hr.tvz.android.onlinemenuapp.order;

import android.content.Intent;

import java.util.List;

public interface OrderContract {
    interface Model {
        interface OnFinishedListener {
            void onFinished(List<String> names, List<String> prices, double finalPrice);

            void onFailure(String error);

            void showToast(int message, int num);
        }

        void getOrderData(Intent intent, OnFinishedListener listener);

        void saveOrderData(OnFinishedListener listener);

        void deleteOrderData(OnFinishedListener listener, int position);
    }

    interface View {
        void setDataToView(List<String> names, List<String> prices, double finalPrice);

        void onResponseFailure(String error);

        void onResponseSuccess(int message, int num);
    }

    interface Presenter {
        void requestOrderData(Intent intent);

        void saveOrderData();

        void deleteOrderData(int position);
    }
}
