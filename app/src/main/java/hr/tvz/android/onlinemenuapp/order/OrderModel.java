package hr.tvz.android.onlinemenuapp.order;

import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hr.tvz.android.onlinemenuapp.menu.MenuActivity;
import hr.tvz.android.onlinemenuapp.model.Dish;
import hr.tvz.android.onlinemenuapp.model.Order;
import hr.tvz.android.onlinemenuapp.network.Service;
import hr.tvz.android.onlinemenuapp.network.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
;
import static hr.tvz.android.onlinemenuapp.R.string.emptyOrderToast;
import static hr.tvz.android.onlinemenuapp.R.string.orderToast;

public class OrderModel implements OrderContract.Model {
    private final String TAG = "OrderModel";
    List<String> dishNames = new ArrayList<>();
    List<String> dishPrices = new ArrayList<>();
    List<Long> dishIds = new ArrayList<>();
    double finalPrice;
    Order order = new Order();

    @Override
    public void getOrderData(Intent intent, OnFinishedListener listener) {
        if (intent != null) {
            ArrayList<Dish> dishes = intent.getParcelableArrayListExtra("dishes");
            finalPrice = intent.getDoubleExtra("price", 1);

            for (Dish dish : dishes) {
                dishNames.add(dish.getName());
                dishPrices.add(dish.getPrice().toString());
                dishIds.add(dish.getId());
            }

            order.setDishIds(dishIds);
            listener.onFinished(dishNames, dishPrices, finalPrice);

        } else {
            listener.onFailure("Couldn't load order data");
        }
    }

    @Override
    public void saveOrderData(OnFinishedListener listener) {
        if (finalPrice != 0.0) {
            Service service = ServiceGenerator.createService(Service.class);
            Call<Order> call = service.saveOrder(order);
            call.enqueue(new Callback<Order>() {
                @Override
                public void onResponse(Call<Order> call, Response<Order> response) {
                    Log.d(TAG, "Order saved with dish ids: " + response.body().getDishIds());
                    listener.showToast(orderToast, 1);
                }

                @Override
                public void onFailure(Call<Order> call, Throwable t) {
                }
            });
        } else {
            listener.showToast(emptyOrderToast, 0);
        }

    }

    @Override
    public void deleteOrderData(OnFinishedListener listener, int position) {
        String priceToDelete = dishPrices.get(position);

        dishNames.remove(position);
        dishPrices.remove(position);

        finalPrice = finalPrice - Double.valueOf(priceToDelete);

        listener.onFinished(dishNames, dishPrices, finalPrice);
    }
}
